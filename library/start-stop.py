#!/usr/bin/python

import os
import re
import signal
import time

from ansible.module_utils.basic import AnsibleModule


class TimeOutException(Exception):
   pass


class bo_object(object):

    def __init__(self, ansible_module):
        self.ansible_module = ansible_module
        self.auto_discover_command = ''
        self.is_still_running_command = ''
        
    def __helper_function(self, command):
        executable_path = ''
        try:
            executable_path = self._execute(command).split()[0]
        except IndexError:
            pass
        return executable_path

    @property
    def auto_discover_install_path(self):
        # In order to get the installation_path I am going 
        # to execute below bash command
        install_path = self.__helper_function(self.auto_discover_command)
        # TODO: I did not find a way to remove all string except ^.*sap_bobj
        # So I am going to add again sap_bobj after removing it
        install_path = re.sub('(sap_bobj.*)', '', install_path)
        return os.path.join(install_path, 'sap_bobj')

    def is_still_running(self):
        # In order to get the installation_path I am going 
        # to execute below bash command
        return self.__helper_function(self.is_still_running_command)

    def start(self):
        pass
    
    def stop(self):
        pass

    def _execute(self, command):
        """
        Execute commands
        """
        status, message, _ = self.ansible_module.run_command(
            args=command, use_unsafe_shell=True
        )
        if status != 0 or 'Warning' in message:
            message = 'Command {command} fails with message: {message}'.format(
                command=command, message=message)
            self.ansible_module.fail_json(msg=message)
        else:
            return message


class bo(bo_object):

    def __init__(self, module):
        super(bo, self).__init__(module)
        self.auto_discover_command = "ps -eo cmd | grep '[s]ap_bobj' | grep -vi tomcat |tail -1"
        self.is_still_running_command = "ps -eo cmd | grep '[s]ap_bobj' | grep -vi tomcat |tail -1"

    def _is_bo_process_running():
        # TODO: Is below step correct? 
        return self.executable_path
 
    def stop(self):
        command = 'stopservers'
        bash_command = os.path.join(self.auto_discover_install_path, command)
        self._execute(bash_command)
        while self.is_still_running():
            pass
        # Remove process that are still in severpids folder
        serverpids_path = os.path.join(
            self.auto_discover_install_path, 'serverpids/*'
        )
        bash_command = ''.join(['rm -f ', serverpids_path])
        self._execute(bash_command)


class tomcat(bo_object):
    
    def __init__(self, module):
        super(tomcat, self).__init__(module)
        self.auto_discover_command = "ps -eo cmd | grep '[t]omcat' | tail -1"
        self.is_still_running_command = "ps -eo cmd | grep '[t]omcat' | tail -1"
    
    def stop(self):
        command = 'tomcatshutdown.sh'
        bash_command = os.path.join(self.auto_discover_install_path, command)
        self._execute(bash_command)
        while self.is_still_running():
            pass
        return


def _alarm_handler(signum, frame):
    raise TimeOutException()

def main():
    module = AnsibleModule(
        argument_spec = dict(
            bo_action = dict(required=True, choices=['start', 'stop',]),
            type      = dict(required=True, choices=['bo', 'tomcat']),
            timeout   = dict(required=False, type='int', default=10, help='Time is in minutes')
        )
    )
    # Setup timeout
    signal.signal(signal.SIGALRM, _alarm_handler)
    signal.alarm(module.params['timeout'] * 60)
    # Create instance depends on type
    if 'tomcat' == module.params['type']:
        object_instance = tomcat(module)
    elif 'bo' == module.params['type']:
        object_instance = bo(module)
    # Execute instrucction but with timeout
    try:
        if 'start' == module.params['bo_action']:
            object_instance.start()
        elif 'stop' == module.params['bo_action']:
            object_instance.stop()
    except TimeOutException:
        module.fail_json(
            msg="It was not possible to stop the servers "
            " Please check the logs for more information"
        )
    # Disable signal alarm
    signal.alarm(0)
    # If installation path was auto discover
    # then the original location is passed as a value for further use
    result = {'installation_path': object_instance.auto_discover_install_path}
    module.exit_json(**result)

if __name__ == '__main__':
      main()